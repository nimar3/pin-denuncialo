// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services', 'ngResource'])

.run(function($ionicPlatform, $cordovaNetwork, $rootScope, ConnectionStatus) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleLightContent();
    }

    if (window.Connection) {
      $rootScope.$on('$cordovaNetwork:online', function(event, networkState) {
        ConnectionStatus.set(true);
      })
      $rootScope.$on('$cordovaNetwork:offline', function(event, networkState) {
        ConnectionStatus.set(false);
      })

      if ($cordovaNetwork.isOffline()) ConnectionStatus.set(false);
    }

    $rootScope.filter = "apoyos";

    variable1 = true;

    $rootScope.$on('filter', function(newFilter, data) {
        $rootScope.filter = data;
        console.log(data);
    });

  });
})

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  // setup an abstract state for the tabs directive
    .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html'
  })

  // Each tab has its own nav history stack:

  .state('tab.dash', {
    url: '/dash',
    views: {
      'tab-dash': {
        templateUrl: 'templates/tab-dash.html',
        controller: 'DashCtrl'
      }
    }
  })

  .state('tab.denuncias', {
    url: '/denuncias',
    cache: false,
    views: {
      'tab-denuncias': {
        templateUrl: 'templates/tab-denuncias.html',
        controller: 'DenunciasCtrl'
      }
    }
  })

  .state('tab.denuncia-detail', {
    url: '/denuncias/:id',
    views: {
      'tab-denuncias': {
        templateUrl: 'templates/denuncia-detail.html',
        controller: 'DenunciaDetailCtrl'
      }
    }
  })

  .state('tab.comentarios', {
    url: '/denuncias/:id/comentarios',
    cache: false,
    views: {
      'tab-denuncias': {
        templateUrl: 'templates/comentarios.html',
        controller: 'ComentariosCtrl'
      }
    }
  })

  .state('tab.nueva-denuncia', {
    url: '/nueva-denuncia',
    views: {
      'tab-nueva-denuncia': {
        templateUrl: 'templates/tab-nueva-denuncia.html',
        controller: 'NuevaDenunciaCtrl'
      }
    }
  })

  .state('tab.no-internet', {
    url: '/no-internet',
    views: {
      'tab-no-internet': {
        templateUrl: 'templates/no-internet.html',
        controller: 'NoInternet'
      }
    }
  })

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/tab/dash');

});
