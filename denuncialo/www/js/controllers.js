angular.module('starter.controllers', ['ngCordova'])

.config(function($compileProvider){
  $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|tel|data):/);
})

.controller('DenunciasCtrl', function($state, $scope, $ionicLoading, $rootScope, Denuncias) { // $state, $window

  $scope.updateFilterOptions = function(newFilter) {
    $rootScope.filter = newFilter;
    $scope.doRefresh();
  }

  $scope.toggle = function() {
    $scope.filterOptions = !$scope.filterOptions;
  };

  var getDenuncias = function() {
    Denuncias.query({filter : $rootScope.filter}).$promise.then(function (listadoDenuncias) {
        $scope.denuncias = listadoDenuncias;
        //ocultamos el spinner de carga y el de refresh
        $ionicLoading.hide();
        $scope.$broadcast('scroll.refreshComplete');
    }, function (err) {
        // Error
        console.log(err);
    });
  }

  $scope.doRefresh = function() {
    getDenuncias();
  };

  //buscamos las denuncias cada vez que se muesta esta TAB, pero antes mostramos el spinner
  $ionicLoading.show({
    animation: 'fade-in',
    showBackdrop: true,
    maxWidth: 200,
    showDelay: 0
  });

  $scope.filtro = "apoyadas";
  $scope.filterOptions = true;
  getDenuncias();

})

.controller('DenunciaDetailCtrl', function($scope, $stateParams, $ionicModal, $ionicLoading, $ionicPopup, Denuncias, Apoyo) {

  //buscamos los datos de la denuncia, pero antes mostramos el spinner
  $ionicLoading.show({
    animation: 'fade-in',
    showBackdrop: true,
    maxWidth: 200,
    showDelay: 0
  });

  Denuncias.get({ id: $stateParams.id }).$promise.then(function (denuncia) {
    $scope.denuncia = denuncia;
    $ionicLoading.hide();
  });

  $scope.showConfirm = function() {
   var confirmPopup = $ionicPopup.confirm({
     title: 'Eliminar Denuncia',
     template: 'Está seguro que desea eliminar la denuncia?'
   });
   confirmPopup.then(function(res) {
     if(res) {
       eliminarDenuncia();
     }
   });
 };

  var eliminarDenuncia = function() {
    Denuncias.remove({ id: $stateParams.id }).$promise.then(function (listadoDenuncias) {
        $state.go('tab.dash');
    }, function (err) {
        // Error
        console.log(err);
    });
  }

  $scope.apoyarDenuncia = function(idDenuncia) {
    $scope.apoyo = new Apoyo();
    $scope.apoyo.id = 1;
    $scope.apoyo.idDenuncia = idDenuncia;

    $scope.apoyo.$save(function() {
      $state.go($state.current, {}, {reload: true});
    });
  }

  //GARBARE, esto hay que cambiarlo por el ionic-scroll para una imagen
  $ionicModal.fromTemplateUrl('image-modal.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modal = modal;
    });

    $scope.openModal = function() {
      $scope.modal.show();
    };

    $scope.closeModal = function() {
      $scope.modal.hide();
    };

    $scope.showImage = function() {
      $scope.openModal();
    }
})

.controller('ComentariosCtrl', function($scope, $state, $stateParams, $window, $ionicLoading, Comentarios) {
  $scope.comentarios = Comentarios.query({ id: $stateParams.id });

  $scope.nuevoComentario = new Comentarios();

  $scope.enviarComentario = function() {

    $ionicLoading.show({
      animation: 'fade-in',
      showBackdrop: true,
      maxWidth: 200,
      showDelay: 0
    });

    $scope.nuevoComentario.userId = '1'; //
    $scope.nuevoComentario.$save(function() {
      $state.go($state.current, {}, {reload: true});
      $ionicLoading.hide();
    });
  };
})

.controller('NuevaDenunciaCtrl', function($scope, $state, $stateParams, $window, $cordovaCamera, $cordovaGeolocation, $ionicLoading, Denuncias, ConnectionStatus) {

  //nueva denuncia
  $scope.denuncia = new Denuncias();
  $scope.denuncia.error = false;

  //geolocalizacion
  $scope.getLocation = function() {
    var optionsGeolocation = {
      timeout: 15000,
      enableHighAccuracy: true
    };

    $cordovaGeolocation.getCurrentPosition(optionsGeolocation).then(function(position){
      //coordenadas de la denuncia
      $scope.denuncia.latitud = position.coords.latitude;
      $scope.denuncia.longitud = position.coords.longitude;
      $scope.denuncia.error = false;
    }, function(error){
      $scope.denuncia.error = "No se pudo obtener su ubicación.";
    });
  }

  $scope.getPhoto = function() {

    //cuando hacemos la foto, buscamos la geolocalizacion del usuario
    $scope.getLocation();

    var optionsPhoto = {
      quality: 50,
      destinationType: Camera.DestinationType.DATA_URL,
      encodingType: Camera.EncodingType.JPEG,
      correctOrientation: true
    };

    $cordovaCamera.getPicture(optionsPhoto).then(function(imageData) {
      $scope.denuncia.imagen = "data:image/jpeg;base64," + imageData;
    }, function(err) {
      // error
      console.log(err);
    });
  };

  $scope.enviarDenuncia = function() {

    if (ConnectionStatus.get() == true) //hay internet?
    {
      if ( ! $scope.denuncia.error && $scope.denuncia.imagen != null && $scope.denuncia.categoria != null) // error = estan las coordenadas, la foto y categoria - CHAPUZA TOTAL
      {
        $ionicLoading.show({
          template: 'Enviando Denuncia...',
          animation: 'fade-in',
          showBackdrop: false,
          maxWidth: 200,
          showDelay: 0
        });

        $scope.denuncia.$save(function() {
          $ionicLoading.hide();

          //borramos los datos del formulario, deberia hacerse con $pristine, no funciona por alguna razon, FUCK IT - MAS CHAPUZA
          $scope.denuncia.imagen = "";
          $scope.denuncia.titulo = "";
          $scope.denuncia.descripcion = "";
          $scope.denuncia.categoria = "";

          $state.go('tab.dash'); // si se envia correctamente volvemos al dash
        });

      }
      else
      {
        console.log("imagen: "+$scope.denuncia.imagen+" error: "+$scope.denuncia.error);
      }
    }
    else
    {
      $state.go('tab.no-internet');
    }

  }

})

.controller('NoInternet', function($scope, $state, $stateParams, $window) {

})

.controller('DashCtrl', function($scope, $ionicLoading, Denuncias) {

  var getDenuncias = function() {
    Denuncias.query().$promise.then(function (listadoDenuncias) {
        $scope.denuncias = listadoDenuncias;
        //ocultamos el spinner de carga y el de refresh
        $ionicLoading.hide();
        $scope.$broadcast('scroll.refreshComplete');
    }, function (err) {
        // Error
        console.log(err);
    });
  }

  $scope.doRefresh = function() {
    getDenuncias();
  };

  //Buscamos las denuncias cada vez que se muesta esta TAB, pero antes mostramos el spinner
  $ionicLoading.show({
    animation: 'fade-in',
    showBackdrop: true,
    maxWidth: 200,
    showDelay: 0
  });

  getDenuncias();
});
