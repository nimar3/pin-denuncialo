angular.module('starter.services', [])

.factory('Camera', ['$q', function($q) {

  return {
    getPicture: function(options) {
      var q = $q.defer();

      navigator.camera.getPicture(function(result) {
        // Do any magic you need
        q.resolve(result);
      }, function(err) {
        q.reject(err);
      }, options);

      return q.promise;
    }
  }

}])

.factory('ConnectionStatus', function() {
  var status = true;

  return {
  	get: function() {
  		return status;
  	},
  	set: function(new_state) {
      status = new_state;
  	}
  }
})

.factory('Apoyo',function($resource){
    return $resource('http://denuncialo.azurewebsites.net/api/apoyar',{},{
        update: {
            method: 'PUT'
        }
    });
})

.factory('Comentarios',function($resource, Filter){
    return $resource('http://denuncialo.azurewebsites.net/api/comentarios/:id',{id:'@_id'},{
        update: {
            method: 'PUT'
        }
    });
})

.factory('Denuncias',function($resource){
    return $resource('http://denuncialo.azurewebsites.net/api/denuncias/:id',{ id:'@_id',  filter: '@_filter' },{
        update: {
            method: 'PUT'
        }
    });
});
